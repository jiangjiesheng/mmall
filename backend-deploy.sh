if [ ! -n "$1" ];
        then
        echo -e "请输入要发布的git版本和tomcat文件夹名"
        echo -e "./backend-deploy.sh v2.0 mm.jiangjiesheng.cn-1"
        echo -e "./backend-deploy.sh v2.0 mm.jiangjiesheng.cn-2"
        exit
fi



echo "===========进入git项目happymmall目录============="
cd /j-deploy/git-repository/mmall


echo "==========git切换分之到mmall-"$1"==============="
git checkout "$1"

echo "==================git fetch======================"
git fetch

echo "==================git pull======================"
git pull


echo "===========编译并跳过单元测试===================="
mvn clean package -Dmaven.test.skip=true -Pprod


echo "============删除旧的ROOT.war==================="
rm /g-software/tomcat7/$2/webapps/ROOT.war


echo "======拷贝编译出来的war包到tomcat下-ROOT.war======="
cp /j-deploy/git-repository/mmall/target/mmall.war  /g-software/tomcat7/$2/webapps/ROOT.war


echo "============删除tomcat下旧的ROOT文件夹============="
rm -rf /g-software/tomcat7/$2/webapps/ROOT



echo "====================关闭tomcat====================="
/g-software/tomcat7/$2/bin/shutdown.sh


echo "================sleep 5s 之前是10秒========================="
for i in {1..10}
do
	echo $i"s"
	sleep 1s
done


echo "====================启动tomcat====================="
/g-software/tomcat7/$2/bin/startup.sh

echo "如果不能访问可以 ps -ef | grep tomcat ,然后kill -9 PID,重新编译"
echo "或者等几分钟后看看能不能自动启动catalina"
echo "找个比较安全的路径执行 wget 127.0.0.1:8080/index.jsp 等待强制初始化" 
