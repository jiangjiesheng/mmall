<h4>在线商城Java项目-架构演进:Tomcat集群与Redis分布式</h4>

    
    初次pull代码
    注意添加jar到项目中 src/main/webapp/lib 和 src/main/webapp/WEB-INF/lib ，分别选中文件夹，右击Add as Library...  
    特别注意datasource.properties中数据库驱动的路径，但是生产环境的路径不是这个好像也没有影响。

<br/>
<div>线上访问 <a href="http://mm.jiangjiesheng.cn" target="_blank">http://mm.jiangjiesheng.cn</a> (暂时取消https，免1年1次申请SSL)</div>
<br/>
<div>如果登录后调用其他的接口还是提示未登录，</div>
<div>注意修改mmall.properties下的cookie.domain为当前环境的ip或域名或localhost</div>
<div>以实际访问url为准</div>
<div>后期如果基于App调用，可以使用token全面更换掉cookies保存SessionId</div>
<div>------------------------------------------------</div>
<div>目前经常出现启动报错(重启又能解决)的情况
    Error creating bean with name 'enableRedisKeyspaceNotificationsInitializer'
    defined in class path resource [org/springframework/session/data/redis/config/annotation/web/http/RedisHttpSessionConfiguration.class]:
    Invocation of init method failed;
    nested exception is org.springframework.data.redis.RedisConnectionFailureException:
    Cannot get Jedis connection; nested exception is redis.clients.jedis.exceptions.JedisConnectionException:
    Could not get a resource from the pool</div>

> 作者：[江节胜](https://www.baidu.com/s?wd=%E6%B1%9F%E8%8A%82%E8%83%9C%20%E8%83%9C%E8%A1%8C%E5%A4%A9%E4%B8%8B%E7%BD%91)

> 微信：**767000122  (欢迎添加好友)**

> Q Q ：596957738

> 个人网站：tech.jiangjiesheng.cn 

> 联系邮箱：dev@jiangjiesheng.cn
