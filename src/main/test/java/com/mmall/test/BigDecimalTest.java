package com.mmall.test;

import org.junit.Test;

import java.math.BigDecimal;

/**
 * Description：
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/18
 */
public class BigDecimalTest {
    @Test
    public void test1() {//单元测试中的返回值一定是void
        System.out.println(0.05 + 0.01);
        System.out.println(1.0 - 0.42);
        System.out.println(4.015 * 100);
        System.out.println(123.3 / 100);
    }
    @Test
    public void test2() {
        BigDecimal b1 =new BigDecimal(0.05);
        BigDecimal b2 =new BigDecimal(0.01);
        System.out.println(b1.add(b2));
    }
    @Test
    public void test3() {
        BigDecimal b1 =new BigDecimal("0.05");
        BigDecimal b2 =new BigDecimal("0.01");
        System.out.println(b1.add(b2));
    }



}
