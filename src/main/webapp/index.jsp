<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8"/>
    <meta name="viewport" content="width=device-width, initial-scale=0.7,minimum-
scale=0.3, maximum-scale=1, user-scalable=yes">

    <!-- 宽度也可设置 width = 1048px -->
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-detection" content="telephone=no"/>

    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="0">
</head>
<body>
<h2>Hello World!</h2>

<h2>------------------</h2>
<h4>如果登录后调用其他的接口还是提示未登录，</h4>
<h4>注意修改mmall.properties下的cookie.domain为当前环境的ip或域名或localhost</h4>
<h4>以实际访问url为准</h4>
<h4>后期如果基于App调用，可以使用token全面更换掉cookies保存SessionId</h4>
<h4>------------------------------------------------</h4>
<h4>目前经常出现启动报错(重启又能解决)的情况
    Error creating bean with name 'enableRedisKeyspaceNotificationsInitializer'
    defined in class path resource [org/springframework/session/data/redis/config/annotation/web/http/RedisHttpSessionConfiguration.class]:
    Invocation of init method failed;
    nested exception is org.springframework.data.redis.RedisConnectionFailureException:
    Cannot get Jedis connection; nested exception is redis.clients.jedis.exceptions.JedisConnectionException:
    Could not get a resource from the pool</h4>
<h4>Redis如果增加密码，applicationContext-spring-session.xml 也要注入密码 </h4>
<h2>------------------</h2>
<h2>tomcat1</h2>
<h2>tomcat1</h2>
<h2>tomcat1</h2>
<h2>tomcat1</h2>
SpringMVC上传
<form name="form1" action="/manage/product/upload.do" method="post" enctype="multipart/form-data">
    <input type="file" name="upload_file"/>
    <input type="submit" value="springmvc上传文件"/>
</form>

富文本图片上传
<form name="form1" action="/manage/product/richtext_img_upload.do" method="post" enctype="multipart/form-data">
    <input type="file" name="upload_file"/>
    <input type="submit" value="富文本图片上传"/>
</form>

</body>
</html>
