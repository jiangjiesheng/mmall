package com.mmall.common;

import com.mmall.util.PropertiesUtil;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.util.Hashing;
import redis.clients.util.Sharded;

import java.util.ArrayList;
import java.util.List;

/**
 * Description：Redis分片连接池
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/2/26
 */
public class RedisShardedPool {
    private static ShardedJedisPool pool;//shard Jedis连接池
    private static Integer maxTotal = Integer.parseInt(PropertiesUtil.getProperty("redis.max.total", "20")); //最大连接数
    private static Integer maxIdle = Integer.parseInt(PropertiesUtil.getProperty("redis.max.idle", "10")); //在Jedispool中最大的idle状态(空闲状态)的jedis实例的个数
    private static Integer minIdle = Integer.parseInt(PropertiesUtil.getProperty("redis.min.idle", "2"));//在Jedispool中最小的idle状态(空闲状态)的jedis实例的个数

    private static Integer timeout = Integer.parseInt(PropertiesUtil.getProperty("redis.timeout"));//redis连接超时 线上最好设置1000*2，测试测试2秒时间容易导致超时

    private static String redis1Ip = PropertiesUtil.getProperty("redis1.ip");
    private static Integer redis1Port = Integer.parseInt(PropertiesUtil.getProperty("redis1.port"));
    private static String redis1PassWd = PropertiesUtil.getProperty("redis1.passwd");

    private static String redis2Ip = PropertiesUtil.getProperty("redis2.ip");
    private static Integer redis2Port = Integer.parseInt(PropertiesUtil.getProperty("redis2.port"));
    private static String redis2PassWd = PropertiesUtil.getProperty("redis2.passwd");

    private static boolean testOnBorrow = Boolean.parseBoolean(PropertiesUtil.getProperty("redis.test.borrow", "true"));//在borrow一个jedis实例的时候，是否要进行验证操作，如果赋值为true，则得到的jedis实例肯定是可以用的
    private static boolean testOnReturn = Boolean.parseBoolean(PropertiesUtil.getProperty("redis.test.return", "true"));//在Return一个jedis实例的时候，是否要进行验证操作，如果赋值为true，则返回的jedis实例肯定是可以用的

    private static void initPool() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMinIdle(minIdle);

        config.setTestOnBorrow(testOnBorrow);
        config.setTestOnReturn(testOnReturn);

        config.setBlockWhenExhausted(true);//连接耗尽时，是否阻塞，false会阻塞直到超时，默认为true
        JedisShardInfo info1 = new JedisShardInfo(redis1Ip, redis1Port, timeout);
        info1.setPassword(redis1PassWd);//设置redis服务器的访问密码
        JedisShardInfo info2 = new JedisShardInfo(redis2Ip, redis2Port, timeout);
        info2.setPassword(redis2PassWd);//设置redis服务器的访问密码
        List<JedisShardInfo> jedisShardInfoList = new ArrayList<JedisShardInfo>();
        jedisShardInfoList.add(info1);
        jedisShardInfoList.add(info2);//这里可以通过配置动态加载，比如使用-连接ip和port，然后分割出来
        pool = new ShardedJedisPool(config, jedisShardInfoList, Hashing.MURMUR_HASH, Sharded.DEFAULT_KEY_TAG_PATTERN);
    }

    static {
        initPool();
    }

    public static ShardedJedis getJedis() {
        return pool.getResource();
    }

    public static void returnBrokenResource(ShardedJedis jedis) {
        pool.returnBrokenResource(jedis);
    }

    public static void returnResource(ShardedJedis jedis) {
        pool.returnResource(jedis);
    }

 /*   public static void main(String[] args) {
        ShardedJedis jedis = pool.getResource();
        for (int i = 0; i < 10; i++) {
            jedis.set("key" + i, "value" + i);
        }
        returnResource(jedis);
        // pool.destroy();//临时调用，销毁连接池中的所有连接
        System.out.println("program is end");
    }*/
}
