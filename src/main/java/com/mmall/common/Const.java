package com.mmall.common;

import com.google.common.collect.Sets;

import java.util.Set;

/**
 * 产量类
 * Created by dev@jiangjiesheng on 2017/11/9.
 */
public class Const {
    public static final String CURRENT_USER = "currentuser";//当前登录用户
    public static final String EMAIL = "email";
    public static final String USERNAME = "username";

    public static String TOKEN_PREFIX = "token_";// token(令牌) 应用:忘记密码

    public interface RedisCacheExTime {
        int REDIS_SESSION_EXTIME = 60 * 30;//session 有效时间 30分钟
    }

    public interface Cart {
        int CHECKED = 1;//购物车中的选中状态
        int UN_CHECKED = 0;//购物车中的未选中状态

        String LIMIT_NUM_FAIL = "LIMIT_NUM_FAIL";
        String LIMIT_NUM_SUCCESS = "LIMIT_NUM_SUCCESS";
    }

    public interface Role {
        int ROLE_CUSTOMER = 0;//普通用户
        int ROLE_ADMIN = 1;//管理员
    }

    public enum ProductStatusEnum {
        ON_SAIL(1, "在线");
        private String value;
        private int code;

        ProductStatusEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }
    }

    public interface ProductListOrderBy {
        //Set时间复杂度O1,List时间复杂度On
        Set<String> PRICE_ASC_DESC = Sets.newHashSet("price_desc", "price_asc");
    }

    public enum OrderStatusEnum {

        CANCELED(0, "已取消"),
        NO_PAY(10, "未支付"),
        PAID(20, "已付款"),
        SHIPPED(40, "已发货"),
        ORDER_SUCCESS(50, "订单完成"),
        ORDER_CLOSE(50, "订单关闭");

        private String value;
        private int code;

        OrderStatusEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }

        public static OrderStatusEnum codeOf(int code) {//返回的是枚举，比较新鲜
            for (OrderStatusEnum orderStatusEnum : values()) {
                if (orderStatusEnum.getCode() == code) {
                    return orderStatusEnum;
                }
            }
            throw new RuntimeException("没有找到对应的枚举");
        }
    }

    public interface AlipayCallback {
        String TRADE_STATUS_WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
        String TRADE_STATUS_TRADE_SUCCESS = "TRADE_SUCCESS";//来自支付宝交易状态说明https://docs.open.alipay.com/194/103296

        String RESPONSE_SUCCESS = "success";//按支付宝要求进行确认
        String RESPONSE_FAILED = "failed";
    }

    public enum PayPlat {// wechat or alipay
        ALIPAY(1, "支付宝");
        private int code;
        private String value;

        PayPlat(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }
    }

    public enum PaymentTypeEnum {// 支付方式
        ONLINE_PAY(1, "在线支付");
        private int code;
        private String value;

        PaymentTypeEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }

        public static PaymentTypeEnum codeOf(int code) {//返回的是枚举，比较新鲜
            for (PaymentTypeEnum paymentTypeEnum : values()) {
                if (paymentTypeEnum.getCode() == code) {
                    return paymentTypeEnum;
                }
            }
            throw new RuntimeException("没有找到对应的枚举");
        }
    }

    public interface REDIS_LOCK {
        String CLOSE_ORDER_TASK_LOCK = "CLOSE_ORDER_TASK_LOCK";
    }
}
