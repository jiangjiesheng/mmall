package com.mmall.common;

/**
 * Description：ServerResponse 中使用到的常量
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/8
 */
public enum ResponseCode {
    SUCCESS(0, "SUCCESS"),
    ERROR(1, "ERROR"),
    NEED_LOGIN(10, "NEED_LOGIN"),//注意这里是逗号
    ILLEGAL_ARGUMENT(2, "ILLEGAL_ARGUMENT"),
    ILLEGAL_REQUEST(3, "ILLEGAL_REQUEST"),
    INNER_ERROR(4, "INNER_ERROR");//这里是分号

    private final int code;
    private final String desc;

    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
