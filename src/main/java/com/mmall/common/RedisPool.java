package com.mmall.common;

import com.mmall.util.PropertiesUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 * Description：Jedis连接池
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/2/19
 */
public class RedisPool {
    private static JedisPool pool;//Jedis连接池
    private static Integer maxTotal = Integer.parseInt(PropertiesUtil.getProperty("redis.max.total", "20")); //最大连接数
    private static Integer maxIdle = Integer.parseInt(PropertiesUtil.getProperty("redis.max.idle", "10")); //在Jedispool中最大的idle状态(空闲状态)的jedis实例的个数
    private static Integer minIdle = Integer.parseInt(PropertiesUtil.getProperty("redis.min.idle", "2"));//在Jedispool中最小的idle状态(空闲状态)的jedis实例的个数

    private static Integer timeout = Integer.parseInt(PropertiesUtil.getProperty("redis.timeout"));//redis连接超时 线上最好设置1000*2，测试测试2秒时间容易导致超时

    private static String redisIp = PropertiesUtil.getProperty("redis1.ip");
    private static Integer redisPort = Integer.parseInt(PropertiesUtil.getProperty("redis1.port"));
    private static String redisPassWd =PropertiesUtil.getProperty("redis1.passwd");

    private static boolean testOnBorrow = Boolean.parseBoolean(PropertiesUtil.getProperty("redis.test.borrow", "true"));//在borrow一个jedis实例的时候，是否要进行验证操作，如果赋值为true，则得到的jedis实例肯定是可以用的
    private static boolean testOnReturn = Boolean.parseBoolean(PropertiesUtil.getProperty("redis.test.return", "true"));//在Return一个jedis实例的时候，是否要进行验证操作，如果赋值为true，则返回的jedis实例肯定是可以用的

    private static void initPool() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMinIdle(minIdle);

        config.setTestOnBorrow(testOnBorrow);
        config.setTestOnReturn(testOnReturn);

        config.setBlockWhenExhausted(true);//连接耗尽时，是否阻塞，false会阻塞直到超时，默认为true
        //pool = new JedisPool(config, redisIp, redisPort, timeout);
        pool = new JedisPool(config, redisIp, redisPort, timeout,redisPassWd);
    }

    static {
        initPool();
    }

    public static Jedis getJedis() {
        /*try {
            return pool.getResource();
            // do redis opt by instance
        } catch (JedisConnectionException e) {
            borroworoprsuccess = false;
            if (jedis != null)
                pool.returnbrokenresource(jedis);

        } finally {
            if (borroworoprsuccess)
                pool.returnresource(jedis);
        }
        jedis = pool.getresource();*/
       return pool.getResource();
    }

    public static void returnBrokenResource(Jedis jedis) {
        pool.returnBrokenResource(jedis);
    }

    public static void returnResource(Jedis jedis) {
        pool.returnResource(jedis);
    }

   public static void main(String[] args) {
        Jedis jedis = pool.getResource();
        jedis.set("jiangkey", "jiangvalue");
        returnResource(jedis);
       // pool.destroy();//临时调用，销毁连接池中的所有连接
        System.out.println("program is end");
    }
}
