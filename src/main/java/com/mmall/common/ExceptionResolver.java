package com.mmall.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Description：全局异常处理
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/3/1
 */
@Slf4j
//@Repository //习惯用在DAO层上
//@Service //习惯用在Service层上
@Component //除以上的其他情况下
public class ExceptionResolver implements HandlerExceptionResolver {


    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {

        log.error("{} Exception ", httpServletRequest.getRequestURI(), e);
        ModelAndView modelAndView = new ModelAndView(new MappingJacksonJsonView());
        //pom.xml中的jackson-mapper-asl
        //当使用时Jackson2.x的时候使用MappingJackson2JsonView，这里使用1.9.x
        modelAndView.addObject("status", ResponseCode.ERROR.getCode());
        modelAndView.addObject("msg", "接口异常,详情请查看服务器端日志");
        modelAndView.addObject("data",e.toString());
        return modelAndView;
    }
}
