package com.mmall.common;

import com.mmall.util.PropertiesUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Description：Redisson框架集成
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/3/4
 */
@Slf4j
@Component
public class RedissonManager {
    private Config config = new Config();
    private Redisson redisson = null;

    private static String redis1Ip = PropertiesUtil.getProperty("redis1.ip");
    private static Integer redis1Port = Integer.parseInt(PropertiesUtil.getProperty("redis1.port"));
    private static String redis1PassWd = PropertiesUtil.getProperty("redis1.passwd");
    private static String redis2Ip = PropertiesUtil.getProperty("redis2.ip");
    private static Integer redis2Port = Integer.parseInt(PropertiesUtil.getProperty("redis2.port"));
    private static String redis2PassWd = PropertiesUtil.getProperty("redis2.passwd");

    public Redisson getRedisson() {
        return redisson;
    }

    //static {  //TODO }
    //保持了方法内操作的唯一性。 适合用一些加载jni操作。 保证只操作一次，类似Application.
    //static{} 内的操作是走在所以当前class 内方法的最前端

    @PostConstruct
    private void init() {//初始化调用,也可使用static{}
        try {//Ctrl Alt T 快速调出
            config.useSingleServer().setPassword(redis1PassWd).setAddress(new StringBuilder().append(redis1Ip).append(":").append(redis1Port).toString());
            redisson = (Redisson) Redisson.create(config);
            log.info("Redisson初始化结束");
        } catch (Exception e) {
            log.error("Redisson init error", e);
            e.printStackTrace();
        }
    }

}
