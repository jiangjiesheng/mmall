package com.mmall.common;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * 利用Guava(LoadingCache) 编写本地缓存
 * 另外也要注意 ，一旦token被获取后，这个token就应该失效机制，标记用户身份的不能失效，
 * 但是标记某一次订单请求的，应该要失效！
 * Created by Administrator on 2017/11/9.
 */
public class TokenCache {
    private static Logger logger = LoggerFactory.getLogger(TokenCache.class);

    public static String TOKEN_PREFIX = "token_";//放到Const中

    //LRU 最近最少使用算法;
    //12小时
    private static LoadingCache<String, String> loadingCache =
            CacheBuilder.newBuilder().
                    initialCapacity(1000).//初始容量
                    maximumSize(10000).//最大容量
                    expireAfterAccess(12, TimeUnit.HOURS).// 缓存有效期12小时
                    build(new CacheLoader<String, String>() {
                        //默认的数据加载实现，当调用get取值的时候，如果key没有对应的值，就调用这个方法进行加载
                        @Override
                        public String load(String string) throws Exception {
                            return "null";//null 换成 "null" 防止空指针
                        }
                    });

    public static void setKey(String key, String value) {

        loadingCache.put(key, value);
    }

    public static String getKey(String key) {
        String value = null;
        try {
            value = loadingCache.get(key);
            if ("null".equals(value)) {
                return null;
            }
            return value;
        } catch (Exception e) {
            logger.error("localCache get error", e);
        }
        return null;
    }
}
