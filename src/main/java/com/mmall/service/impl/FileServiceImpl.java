package com.mmall.service.impl;

import com.google.common.collect.Lists;
import com.mmall.service.IFileService;
import com.mmall.util.FTPUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Description：处理文件上传（基于SpringMVC）
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/27
 */
@Service("iFileService")
public class FileServiceImpl implements IFileService {
    private Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    /**
     *
     * @param tempUploadedFile
     * @param tempUploadedFileFolder
     * @param ftpTargetFolder ftp服务器下的子路径
     * @return
     */
    public String upload(MultipartFile tempUploadedFile, String tempUploadedFileFolder,String ftpTargetFolder) {
        String fileName = tempUploadedFile.getOriginalFilename();
        String fileExtensionName = fileName.substring(fileName.lastIndexOf(".") + 1);
        String uploadFileName = UUID.randomUUID().toString() + "." + fileExtensionName;
        logger.info("开始上传文件，上传的文件名:{},上传的路径是:{},新文件名：{}", fileName, tempUploadedFileFolder, uploadFileName);
        File fileDir = new File(tempUploadedFileFolder);
        if (!fileDir.exists()) {
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }
        File targetFile = new File(tempUploadedFileFolder, uploadFileName);
        try {
            tempUploadedFile.transferTo(targetFile);
            //上传文件成功
            // 将targetFile上传到FTP服务器
            FTPUtil.uploadFile(ftpTargetFolder, Lists.newArrayList(targetFile));
            // 上传完之后删除upload下面的文件
            targetFile.delete();
        } catch (IOException e) {
            logger.error("上传文件异常", e);
            e.printStackTrace();
            return null;
        }
        return targetFile.getName();
    }
}
