package com.mmall.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Description：
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/27
 */
public interface IFileService {
    String upload(MultipartFile tempUploadedFile, String tempUploadedFileFolder, String ftpTargetFolder);
}
