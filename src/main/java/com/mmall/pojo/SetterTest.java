package com.mmall.pojo;

import lombok.Setter;

/**
 * Description：仅仅用于lombok 后的反编译测试
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/2/14
 */
@Setter
public class SetterTest {
    private String settername;
}


