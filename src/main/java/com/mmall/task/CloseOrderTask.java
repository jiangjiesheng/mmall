package com.mmall.task;

import com.mmall.common.Const;
import com.mmall.common.RedissonManager;
import com.mmall.service.IOrderService;
import com.mmall.util.PropertiesUtil;
import com.mmall.util.RedisShardedPoolUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.concurrent.TimeUnit;

/**
 * Description：定时关单任务
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/3/3
 */
@Slf4j
@Component
public class CloseOrderTask {

    @Autowired
    private IOrderService iOrderService;

    @Autowired
    private RedissonManager redissonManager;

    @PreDestroy
    public void delLock() {
        RedisShardedPoolUtil.del(Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
    }


    // @Scheduled(cron = "0 */1 * * * ?") //每分钟执行一次
    public void closeOrderTaskV1() {
        log.info("------------关闭订单定时任务启动------------");
        int hour = Integer.parseInt(PropertiesUtil.getProperty("close.order.task.time.hour", "2"));
        iOrderService.closeOrder(hour);
        log.info("------------关闭订单定时任务结束------------");
    }

    //@Scheduled(cron = "0 */1 * * * ?") //每分钟执行一次
    public void closeOrderTaskV2() {
        log.info("------------关闭订单定时任务启动------------");
        Long lockTimeout = Long.parseLong(PropertiesUtil.getProperty("lock.timeout", "5000"));
        /**
         * 核心原理
         */
        Long setnxResult = RedisShardedPoolUtil.setnx(Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK, String.valueOf(System.currentTimeMillis() + lockTimeout));
        //只有key值的话，不能set成功，据此判断当前以后有tomcat服务器在执行定时关单任务
        if (setnxResult != null && setnxResult.intValue() == 1) {
            //如果返回值为1，代表设置成功，获取锁(可写)
            closeOrder(Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
        } else {
            log.info("------------没有获得分布式锁:{}", Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
        }
        log.info("------------关闭订单定时任务结束------------");
    }

    /**
     * 自己实现的
     */
    @Scheduled(cron = "0 */1 * * * ?") //每分钟执行一次 生产环境送V3
    public void closeOrderTaskV3() { //
        log.info("------------关闭订单定时任务启动------------");
        Long lockTimeout = Long.parseLong(PropertiesUtil.getProperty("lock.timeout", "5000"));
        /**
         * 核心原理
         */
        Long setnxResult = RedisShardedPoolUtil.setnx(Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK, String.valueOf(System.currentTimeMillis() + lockTimeout));
        //只有key值的话，不能set成功，据此判断当前以后有tomcat服务器在执行定时关单任务
        if (setnxResult != null && setnxResult.intValue() == 1) {
            //如果返回值为1，代表设置成功，获取锁(可写)
            closeOrder(Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
        } else {
            //log.info("------------没有获得分布式锁:{}", Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
            //核心：未获取到锁，继续判断，判断时间戳，看是否可以重置并获取到锁
            String lockValueStr = RedisShardedPoolUtil.get(Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
            if (lockValueStr != null && System.currentTimeMillis() > Long.parseLong(lockValueStr)) {
                //这时候锁可以失效了。
                String getSetResult = RedisShardedPoolUtil.getSet(Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK, String.valueOf(System.currentTimeMillis() + lockTimeout));
                // lockValueStr 与 getSetResult可能是不相等的，redis分布式环境下
                // getSetResult 是获取的最新值
                // 这里set了一个新的值，并获取了一个旧的值
                if (getSetResult == null || (getSetResult != null && StringUtils.equals(lockValueStr, getSetResult))) {
                    //正在获取到锁
                    closeOrder(Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
                } else {
                    log.info("------------没有获得分布式锁:{}", Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
                }
            } else {
                log.info("------------没有获得分布式锁:{}", Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
            }

        }
        log.info("------------关闭订单定时任务结束------------");
    }

    /**
     * 使用Redisson处理
     */
   //@Scheduled(cron = "0 */1 * * * ?") //每分钟执行一次
    public void closeOrderTaskV4() {
        //RLock 分布式锁
        RLock lock = redissonManager.getRedisson().getLock(Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK);
        Boolean getLock = false;
        try {
            //long waitTime, long leaseTime, TimeUnit unit
            //waitTime 设置>0的值时还是有可能分布式Redis都获取到锁。所有使用0
            //waitTime 在这里可以解释为关单任务执行的总时间
            if (getLock = lock.tryLock(0, 50, TimeUnit.SECONDS)) {
                log.info("------------Redisson获取搭到分布式锁:{}，ThreadName:{}", Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK, Thread.currentThread().getName());
                int hour = Integer.parseInt(PropertiesUtil.getProperty("close.order.task.time.hour", "2"));
                iOrderService.closeOrder(hour);//直接关单
            } else {
                log.info("------------Redisson没有获取到分布式锁:{}，ThreadName:{}", Const.REDIS_LOCK.CLOSE_ORDER_TASK_LOCK, Thread.currentThread().getName());
            }
        } catch (InterruptedException e) {
            log.error("Redisson分布式锁获取异常", e);
            e.printStackTrace();
        } finally {
            if (!getLock) {//等待下一次任务调度
                return;
            }
            lock.unlock();//已经获取到锁时要释放
            log.error("Redisson分布式锁释放锁");
            // 0607时间
        }
    }

    private void closeOrder(String lockName) {
        RedisShardedPoolUtil.expire(lockName, 5);//5秒，此值可以根据实际情况调整，防止死锁 ，但是这里依然可能会有死锁发生(例如代码没有走到这里时，tomcat服务器关闭了)
        //可使用@PreDestroy 删除锁，(tomcat shutdown之前执行此注解方法 ，但是如果需要删除的锁非常多，则执行很慢，另外如果直接kill tomcat的进程，则该方法不会执行)
        log.info("------------获取{}，ThreadName:{}------------", lockName, Thread.currentThread().getName());
        int hour = Integer.parseInt(PropertiesUtil.getProperty("close.order.task.time.hour", "2"));
        iOrderService.closeOrder(hour);
        RedisShardedPoolUtil.del(lockName);
        log.info("------------释放{}，ThreadName:{}------------", lockName, Thread.currentThread().getName());
        log.info("===============================================");
    }
}
