package com.mmall.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;

import java.text.SimpleDateFormat;

/**
 * Description：Json序列化工具
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/2/19
 */
@Slf4j
public class JsonUtil {
    //java序列化/反序列化之xstream、protobuf、protostuff 的比较与使用例子
    //https://www.cnblogs.com/xiaoMzjm/p/4555209.html
    private static ObjectMapper objectMapper = new ObjectMapper();

    static {
        //对象的所有字段全部列入
        objectMapper.setSerializationInclusion(Inclusion.ALWAYS);//NON_NULL 不显示null的字段 NON_DEFAULT，NON_EMPTY
        //取消默认转换timestamps形式
        objectMapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);//把DateTime格式化成需要时间样式
        //忽略空Bean转Json的错误
        objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
        //所有的日期格式都统一为以下的样式
        objectMapper.setDateFormat(new SimpleDateFormat(DateTimeUtil.STANDARD_FORMAT));//把DateTime格式化成需要时间样式
        //忽略 在json字符串中存在但是在java对象中不存在对应属性的情况，防止错误
        objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static <T> String obj2String(T obj) {
        if (obj == null) {
            return null;
        }
        try {
            return obj instanceof String ? (String) obj :
                    objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            log.warn("Parse object to String error", e);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 返回格式化好的
     *
     * @param obj
     * @param <T>
     * @return
     */
    public static <T> String obj2StringPretty(T obj) {
        if (obj == null) {
            return null;
        }
        try {
            return obj instanceof String ? (String) obj :
                    objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (Exception e) {
            log.warn("Parse object to String error", e);
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T stringToObj(String str, Class<T> clazz) {
        if (StringUtils.isEmpty(str) || clazz == null) {
            return null;
        }
        try {
            return clazz.equals(String.class) ? (T) str : objectMapper.readValue(str, clazz);
        } catch (Exception e) {
            log.warn("String object to Obj error", e);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 封装性更强1
     *
     * @param str
     * @param typeReference
     * @param <T>
     * @return
     */
    public static <T> T stringToObj(String str, TypeReference<T> typeReference) {
        if (StringUtils.isEmpty(str) || typeReference == null) {
            return null;
        }
        try {
            return typeReference.getType().equals(String.class) ? (T) str : (T) objectMapper.readValue(str, typeReference);
        } catch (Exception e) {
            log.warn("String object to Obj error", e);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 封装性更强2
     *
     * @param str
     * @param colectionClass
     * @param elementClasses 可变参数
     * @param <T>
     * @return
     */
    public static <T> T stringToObj(String str, Class<?> colectionClass, Class<?>... elementClasses) {
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(colectionClass, elementClasses);

        try {
            return objectMapper.readValue(str, javaType);
        } catch (Exception e) {
            log.warn("String object to Obj error", e);
            e.printStackTrace();
            return null;
        }
    }

    //单步调试技巧:光标移动到要调试的对象上，Ctrl+U，在弹出的面板中可以输入表达式，例如.size() ，get(0)
    /*public static void main(String[] args) {
        User user1 = new User();
        user1.setId(1);
        user1.setEmail("admin@jiangjiesheng.cn");

        User user2 = new User();
        user2.setId(2);
        user2.setEmail("admin2@jiangjiesheng.cn");

        String user1Json = JsonUtil.obj2String(user1);
        String user1JsonPretty = JsonUtil.obj2StringPretty(user1);
        log.info("user1json:{}", user1Json);
        log.info("user1jsonPretty:{}", user1JsonPretty);
        User user = JsonUtil.stringToObj(user1JsonPretty, User.class);

        List<User> usersList = Lists.newArrayList();
        usersList.add(user1);
        usersList.add(user2);
        String userlistStr = JsonUtil.obj2StringPretty(usersList);
        log.info("userlistStr:{}", userlistStr);

        List<User> userListObj = JsonUtil.stringToObj(userlistStr, new TypeReference<List<User>>() {
            //空实现
        });

        List<User> userListObj2 = JsonUtil.stringToObj(userlistStr, List.class, User.class);

        System.out.println("end");
    }*/
}
