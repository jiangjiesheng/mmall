package com.mmall.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Description：
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/2/19
 */
@Slf4j
public class CookieUtil {

    private final static String COOKIE_DOMAIN = PropertiesUtil.getProperty("cookie.domain");// .gesturectrl.com 192.168.43.248
    private final static String COOKIE_DOMAIN_IP = PropertiesUtil.getProperty("cookie.domain.ip");// .gesturectrl.com 192.168.43.248
    private final static String COOKIE_NAME = PropertiesUtil.getProperty("cookie.name");//mmall_login_token

    public static String readLoginToken(HttpServletRequest request) {
        return doReadLoginToken(request);
    }

    private static String doReadLoginToken(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie ck : cookies) {
                log.info("cookieName :{},cookieValue:{}", ck.getName(), ck.getValue());
                if (StringUtils.equals(ck.getName(), COOKIE_NAME)) {
                    log.info("return cookieName :{},cookieValue:{}", ck.getName(), ck.getValue());
                    return ck.getValue();
                }
            }
        }
        return null;
    }

    public static void writeLoginToken(HttpServletResponse response, String token) {
        if(StringUtils.isNotBlank(COOKIE_DOMAIN_IP)){
            doWriteOneLoginToken(response, token,COOKIE_DOMAIN_IP);//新增方便ip访问时也能读取
        }
        doWriteOneLoginToken(response, token,COOKIE_DOMAIN);//原始的
    }


    public static void delLoginToken(HttpServletRequest request, HttpServletResponse response) {

        if(StringUtils.isNotBlank(COOKIE_DOMAIN_IP)){
            doDelLoginToken(request, response,COOKIE_DOMAIN_IP);//新增方便ip访问时也能读取
        }
        doDelLoginToken(request, response,COOKIE_DOMAIN); //原始的
    }

    private static void doDelLoginToken(HttpServletRequest request, HttpServletResponse response,String cookieDomain) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie ck : cookies) {
                if (StringUtils.equals(ck.getName(), COOKIE_NAME)) {
                    ck.setDomain(cookieDomain);
                    ck.setPath("/");
                    ck.setMaxAge(0);//0 表示删除此cookie
                    log.info("del cookieName:{},cookieValue:{}", ck.getName(), ck.getValue());
                    response.addCookie(ck);
                    return;//命中后不再循环
                }
            }
        }
    }

    //增加一个带ip的cookie
    private static void doWriteOneLoginToken(HttpServletResponse response, String token ,String cookieDomain) {
        Cookie ck = new Cookie(COOKIE_NAME, token);
        ck.setDomain(cookieDomain);
        ck.setHttpOnly(true);//屏蔽使用脚本获取cookie信息
        ck.setPath("/");
        //如果这个maxage不设置的话，cookie就不会写入硬盘，而是写在内存。只在当前页有效
        ck.setMaxAge(60 * 60 * 24 * 365);//如果是-1，代表永久，0表示删除，单位秒
        log.info("write cookieName:{},cookieValue:{}", ck.getName(), ck.getValue());
        response.addCookie(ck);
    }

}
