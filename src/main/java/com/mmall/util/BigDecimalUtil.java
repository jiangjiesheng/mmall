package com.mmall.util;

import java.math.BigDecimal;

/**
 * Description：封装BigDecimal的String构造器
 * 解决在商业计算过程中float和double丢失精度的问题
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/18
 */
public class BigDecimalUtil {

    private BigDecimalUtil() {//构造器都改成私有的，避免送new
    }

    public static BigDecimal add(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.add(b2);
    }

    public static BigDecimal sub(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.subtract(b2);
    }

    public static BigDecimal mul(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.multiply(b2);
    }

    public static BigDecimal div(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.divide(b2, 2, BigDecimal.ROUND_HALF_UP);//参数2表示小数点个数
        //除不尽的情况 ，看重载方法 中的roundingMode参数
        //有以下几种策略
        // ROUND_UP
        // ROUND_DOWN
        // ROUND_CEILING
        // ROUND_FLOOR
        // ROUND_HALF_UP 四舍五入
        // ROUND_HALF_DOWN
        // ROUND_HALF_EVEN
        // ROUND_UNNECESSARY
    }

}
