package com.mmall.util;

import com.mmall.common.RedisShardedPool;
import com.mmall.common.RedisShardedPool;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;

/**
 * Description：RedisSharded连接池封装-RedisShardedPoolUtil
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/2/19
 */
@Slf4j
public class RedisShardedPoolUtil {
    /**
     * 设置key的有效期，单位是秒
     *
     * @param key
     * @param exTime
     * @return 返回影响的数据的个数（key值唯一，所以返回值应该只有"0"和"1"两种结果）
     */
    public static Long expire(String key, int exTime) {
        ShardedJedis jedis = null;
        Long result = null;
        if (StringUtils.isBlank(key)) {
            return result;
        }
        //Ctrl Alt T
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.expire(key, exTime);
        } catch (Exception e) {
            log.error("expire key:{}  error {}", key, e);
            // e.printStackTrace();
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }

    /**
     * @param key
     * @param value
     * @param exTime
     * @return 返回 OK 或者 null
     */
    public static String setEx(String key, String value, int exTime) {
        ShardedJedis jedis = null;
        String result = null;
        if (StringUtils.isBlank(key) || value == null) {// value不能为null
            return result;
        }
        //Ctrl Alt T
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.setex(key, exTime, value);
        } catch (Exception e) {
            log.error("setEx key:{} value:{} error {}", key, value, e);
            // e.printStackTrace();
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }

    /**
     * @param key
     * @param value
     * @return 返回 OK 或者 null
     */
    public static String set(String key, String value) {
        ShardedJedis jedis = null;
        String result = null;
        if (StringUtils.isBlank(key) || value == null) {
            return result;
        }
        //Ctrl Alt T
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.set(key, value);
        } catch (Exception e) {
            log.error("set key:{} value:{} error {}", key, value, e);
            // e.printStackTrace();
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }
    /**
     * 具有原子性，已存在key则set失败
     * @param key
     * @param value
     * @return 返回 OK 或者 null
     */
    public static Long setnx(String key, String value) {
        ShardedJedis jedis = null;
        Long result = null;
        if (StringUtils.isBlank(key) || value == null) {
            return result;
        }
        //Ctrl Alt T
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.setnx(key, value);
        } catch (Exception e) {
            log.error("setnx key:{} value:{} error {}", key, value, e);
            // e.printStackTrace();
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }

    /**
     * 设置一个新值，要立刻拿到返回值（返回值是旧值） 具有原子性
     * @param key
     * @param value
     * @return 返回 OK 或者 null
     */
    public static String getSet(String key, String value) {
        ShardedJedis jedis = null;
        String result = null;
        if (StringUtils.isBlank(key) || value == null) {
            return result;
        }
        //Ctrl Alt T
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.getSet(key, value);
        } catch (Exception e) {
            log.error("set getset:{} value:{} error {}", key, value, e);
            // e.printStackTrace();
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }

    /**
     *
     * @param key
     * @return 返回 具体value或者null
     */
    public static String get(String key) {
        ShardedJedis jedis = null;
        String result = null;
        if (StringUtils.isBlank(key)) {
            return result;
        }
        //Ctrl Alt T
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.get(key);
        } catch (Exception e) {
            log.error("get key:{} error {}", key, e);
            // e.printStackTrace();
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }

    /**
     * @param key
     * @return 返回影响数据的个数
     */
    public static Long del(String key) {
        ShardedJedis jedis = null;
        Long result = null;
        if (StringUtils.isBlank(key)) {
            return result;
        }
        //Ctrl Alt T
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.del(key);
        } catch (Exception e) {
            log.error("del key:{} error {}", key, e);
            // e.printStackTrace();
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }
/*
    public static void main(String[] args) {
        ShardedJedis jedis = RedisShardedPool.getJedis();
        RedisShardedPoolUtil.set("keyTest", "value");
        String value = RedisShardedPoolUtil.get("keyTest");
        RedisShardedPoolUtil.setEx("keyex1", "valueex", 60 * 10);
        RedisShardedPoolUtil.expire("keyTest1", 60 * 20);
        RedisShardedPoolUtil.del("keyTest");
        RedisShardedPoolUtil.get("xxx");
        System.out.println("end");
    }*/
}
