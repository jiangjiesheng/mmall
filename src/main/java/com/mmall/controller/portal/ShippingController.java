package com.mmall.controller.portal;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Shipping;
import com.mmall.pojo.User;
import com.mmall.service.IShippingService;
import com.mmall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Description：收货地址模块
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/20
 */
@Controller
@RequestMapping("/shipping/")
public class ShippingController {

    @Autowired
    private IShippingService iShippingService;
    @Autowired
    private IUserService iUserService;

    @RequestMapping("add.do")
    @ResponseBody
    public ServerResponse add(/*HttpSession session,*/ HttpServletRequest request, Shipping shipping) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iShippingService.add(user.getId(), shipping);
    }

    @RequestMapping("del.do")
    @ResponseBody
    public ServerResponse add(/*HttpSession session,*/ HttpServletRequest request, Integer shippingId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iShippingService.del(user.getId(), shippingId);
    }

    @RequestMapping("update.do")
    @ResponseBody
    public ServerResponse update(/*HttpSession session,*/ HttpServletRequest request, Shipping shipping) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iShippingService.update(user.getId(), shipping);
    }

    @RequestMapping("select.do")
    @ResponseBody
    public ServerResponse<Shipping> select(/*HttpSession session,*/ HttpServletRequest request, Integer shippingId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iShippingService.select(user.getId(), shippingId);
    }

    @RequestMapping("list.do")
    @ResponseBody
    public ServerResponse<PageInfo> list(/*HttpSession session,*/ HttpServletRequest request, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iShippingService.list(user.getId(), pageNum, pageSize);
    }
}
