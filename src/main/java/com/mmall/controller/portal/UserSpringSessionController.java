package com.mmall.controller.portal;

import com.mmall.common.Const;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import com.mmall.util.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import com.mmall.util.RedisPoolUtil;

/**
 * Description：使用SpringSession框架做测试专用
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/8
 */
//@RestController
@Controller
@RequestMapping("/user/springsession/")
public class UserSpringSessionController {

    @Autowired
    private IUserService iUserService;

    /**
     * 用户登录
     *
     * @param username
     * @param password
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> login(String username, String password, HttpSession session, HttpServletRequest request, HttpServletResponse httpServletResponse/*, HttpServletRequest httpServletRequest*/) {

        //测试全局异常
        // int i = 0;
        // int j = 666 / i;

        //service --> mybatis --> dao
        ServerResponse<User> response = iUserService.login(username, password);
        if (response.isSuccess()) {
            session.setAttribute(Const.CURRENT_USER, response.getData());// 三期注释掉 新增Redis服务

            CookieUtil.writeLoginToken(httpServletResponse, session.getId());
            /*String executionResult = RedisShardedPoolUtil.setEx(session.getId(), JsonUtil.obj2String(response.getData()), Const.RedisCacheExTime.REDIS_SESSION_EXTIME);
            if (executionResult == null) {//Redis服务未启动时 正常返回OK
                response = ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), "Redis服务异常，保存数据失败");
            }*/
        } else {
            session.removeAttribute(Const.CURRENT_USER);//已经登录的，再次登录时
            //涉及到需要登录的接口应该统一设计一下密码修改状态的监听
        }
        return response;
    }

    @RequestMapping(value = "logout.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> logout(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        session.removeAttribute(Const.CURRENT_USER);//三期代码注释
        CookieUtil.delLoginToken(request, response);//Cookie中删除

      /*  String loginToken = CookieUtil.readLoginToken(request);
        CookieUtil.delLoginToken(request, response);//Cookie中删除
        //RedisPoolUtil --> RedisShardedPoolUtil (换成Redis集群)
        RedisShardedPoolUtil.del(loginToken);//Redis中删除*/
        return ServerResponse.createBySuccess();
    }


    @RequestMapping(value = "get_user_info.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> getUserInfo(HttpSession session, HttpServletRequest request) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //User user = iUserService.getUserByRedisCache(request).getData();
        if (user != null)
            return ServerResponse.createBySuccess(user);
        return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户的信息");
    }


}
