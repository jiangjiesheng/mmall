package com.mmall.controller.portal;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.demo.trade.config.Configs;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IOrderService;
import com.mmall.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Iterator;
import java.util.Map;

/**
 * Description：订单创建 支付
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/12/10
 */
@Controller
@RequestMapping("/order/")
public class OrderController {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
    @Autowired
    private IOrderService iOrderService;
    @Autowired
    private IUserService iUserService;

    @RequestMapping("create.do")//创建订单的接口
    @ResponseBody
    public ServerResponse create(/*HttpSession session,*/ HttpServletRequest request, Integer shippingId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iOrderService.createOrder(user.getId(), shippingId);
    }

    @RequestMapping("cancel.do")//取消订单的接口
    @ResponseBody
    public ServerResponse cancel(/*HttpSession session,*/ HttpServletRequest request, Long orderNo) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iOrderService.cancel(user.getId(), orderNo);
    }

    @RequestMapping("get_order_cart_product.do")//获取购物车中产品的接口（购物车中十件选五件，用于展示这五件的详情）
    @ResponseBody
    public ServerResponse getOrderCartProduct(/*HttpSession session,*/ HttpServletRequest request) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iOrderService.getOrderCartProduct(user.getId());
    }

    @RequestMapping("detail.do")// 订单详情的接口
    @ResponseBody
    public ServerResponse detail(/*HttpSession session,*/ HttpServletRequest request, Long orderNo) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iOrderService.getOrderDetail(user.getId(), orderNo);
    }

    @RequestMapping("list.do")//个人中心查看个人订单的接口
    @ResponseBody
    public ServerResponse<PageInfo> list(/*HttpSession session,*/ HttpServletRequest request, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum, @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iOrderService.getOrderList(user.getId(), pageNum, pageSize);
    }

    @RequestMapping("pay.do")
    @ResponseBody
    public ServerResponse pay(/*HttpSession session,*/ Long orderNo, HttpServletRequest request) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        String path = request.getSession().getServletContext().getRealPath("upload");  //发布后在webapp下  这里实测path = null
        path = this.getClass().getResource("/").getPath().replaceAll("^\\/", "") + "upload";
        return iOrderService.pay(orderNo, user.getId(), path);
    }

    @RequestMapping("alipay_callback.do")//支付宝回调的应答 success or failed
    @ResponseBody
    public Object alipayCallback(HttpServletRequest request) {
        Map<String, String> params = Maps.newHashMap();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                //整体效果实现使用逗号拼接出values
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        logger.info("支付宝回调,{},trade_status:{},参数：{}", params.get("sign"),
                params.get("trade_status"), params.toString());
        //异步返回结果的验签(验证回调的正确性，是不是支付宝发的，并且要避免重复通知)
        //https://docs.open.alipay.com/194/103296
        //Ctrl Shift T --> Search AlipaySignature
        //Ctrl o ,继续输入rsaCheckV2 （打开重载的第一个）--> 追加使用StringBuffer，并发时线程安全(但是效率低)
        //支付宝demo默认的验证签名的rsaCheckContent中的类型是SHA1WithRSA
        // （java.security.Signature signature = java.security.Signature.getInstance(AlipayConstants.SIGN_ALGORITHMS); ）
        //而不是RSA2
        //继续看rsaCheckV2重载的第二个方法  rsaCheckV2(Map<String, String> params, String publicKey,String charset,String signType)
        //可以指定signType签名类型 最终调用的是SHA256WithRSA
        //注意 publicKey是支付宝公钥不是商家公钥，沙箱可以看到https://sandbox.alipaydev.com/user/accountDetails.htm
        //按照支付宝验签的要求，需要去掉sign和sign_type两个字段,sign字段支付宝在sdk中已经remove,但是sign_type没有去掉，其他的第一至第四步都已经在SDK中处理过
        //商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，并判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
        // 同时需要校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email），
        // 上述有任何一个验证不通过，则表明本次通知是异常通知，务必忽略。在上述验证通过后商户必须根据支付宝不同类型的业务通知，正确的进行不同的业务处理，并且过滤重复的通知结果数据。
        // 在支付宝的业务通知中，只有交易通知状态为TRADE_SUCCESS或TRADE_FINISHED时，支付宝才会认定为买家付款成功。
        params.remove("sign_type");
        try {
            boolean alipayRSACheckedV2 = AlipaySignature.rsaCheckV2(params, Configs.getAlipayPublicKey(), "utf-8", Configs.getSignType());
            if (!alipayRSACheckedV2) {
                return ServerResponse.createByErrorMessage("非法请求，验证不通过");
            }
        } catch (AlipayApiException e) {
            logger.error("支付宝验证回调异常", e);
            e.printStackTrace();
        }
        //TODO 验证各种数据
        ServerResponse serverResponse = iOrderService.aliCallback(params);
        if (serverResponse.isSuccess()) {
            return Const.AlipayCallback.RESPONSE_SUCCESS;
        }
        return Const.AlipayCallback.RESPONSE_FAILED;
    }

    @RequestMapping("query_order_pay_status.do")//轮询查订单的支付状态
    @ResponseBody
    public ServerResponse<Boolean> queryOrderPayStatus(/*HttpSession session,*/ HttpServletRequest request, Long orderNo) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        ServerResponse serverResponse = iOrderService.queryOrderPayStatus(user.getId(), orderNo);
        if (serverResponse.isSuccess()) {
            return ServerResponse.createBySuccess(true);
        }
        return ServerResponse.createBySuccess(false);//不需要报错
    }
}

