package com.mmall.controller.portal;

import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.ICartService;
import com.mmall.service.IUserService;
import com.mmall.vo.CartVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Description：购物车
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/15
 */
@Controller
@RequestMapping("/cart/")
public class CartController {

    @Autowired
    private ICartService iCartService;
    @Autowired
    private IUserService iUserService;

    @RequestMapping("list.do")
    @ResponseBody
    public ServerResponse<CartVo> list(/*HttpSession session,*/ HttpServletRequest request) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iCartService.list(user.getId());
    }

    @RequestMapping("add.do")
    @ResponseBody
    public ServerResponse<CartVo> add(/*HttpSession session,*/ HttpServletRequest request, Integer count, Integer productId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iCartService.add(user.getId(), productId, count);
    }


    @RequestMapping("update.do")
    @ResponseBody
    public ServerResponse<CartVo> update(/*HttpSession session,*/ HttpServletRequest request, Integer count, Integer productId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iCartService.update(user.getId(), productId, count);
    }

    @RequestMapping("delete_product.do")
    @ResponseBody
    public ServerResponse<CartVo> deleteProduct(/*HttpSession session,*/ HttpServletRequest request, String productIds) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iCartService.deleteProduct(user.getId(), productIds);
    }

    @RequestMapping("select_all.do")//全选
    @ResponseBody
    public ServerResponse<CartVo> selectAll(/*HttpSession session,*/ HttpServletRequest request) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iCartService.selectOrUnselect(user.getId(), null, Const.Cart.CHECKED);
    }

    @RequestMapping("un_select_all.do")//全反选
    @ResponseBody
    public ServerResponse<CartVo> unSelectAll(/*HttpSession session,*/ HttpServletRequest request) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iCartService.selectOrUnselect(user.getId(), null, Const.Cart.UN_CHECKED);
    }

    @RequestMapping("select.do")//单独选
    @ResponseBody
    public ServerResponse<CartVo> select(/*HttpSession session,*/ HttpServletRequest request, Integer productId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iCartService.selectOrUnselect(user.getId(), productId, Const.Cart.CHECKED);
    }

    @RequestMapping("un_select.do")//单独反选
    @ResponseBody
    public ServerResponse<CartVo> unSelect(/*HttpSession session,*/ HttpServletRequest request, Integer productId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return iCartService.selectOrUnselect(user.getId(), productId, Const.Cart.UN_CHECKED);
    }

    @RequestMapping("get_cart_product_count.do")//全选
    @ResponseBody
    public ServerResponse<Integer> getCartProductCount(/*HttpSession session,*/ HttpServletRequest request) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createBySuccess(0);
        }
        return iCartService.getCartProductCount(user.getId());
    }

}
