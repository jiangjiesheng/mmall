package com.mmall.controller.portal;

import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import com.mmall.util.CookieUtil;
import com.mmall.util.JsonUtil;
//import com.mmall.util.RedisPoolUtil;
import com.mmall.util.RedisShardedPoolUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Description：
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/8
 */
//@RestController
@Controller
@RequestMapping("/user/")
public class UserController {

    @Autowired
    private IUserService iUserService;

    /**
     * 用户登录
     *
     * @param username
     * @param password
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> login(String username, String password, HttpSession session, HttpServletRequest request, HttpServletResponse httpServletResponse/*, HttpServletRequest httpServletRequest*/) {
        //service --> mybatis --> dao
        ServerResponse<User> response = iUserService.login(username, password);
        if (response.isSuccess()) {
            //session.setAttribute(Const.CURRENT_USER, response.getData());// 三期注释掉 新增Redis服务
            //Const.CURRENT_USER 为自定义产量类，后期应该和cookie结合，
            //有cookie时也给登录并写入到session，这一过程都统一在拦截器中处理
            CookieUtil.writeLoginToken(httpServletResponse, session.getId());
            //CookieUtil.readLoginToken(httpServletRequest);
            //CookieUtil.delLoginToken(httpServletRequest,httpServletResponse);
            //RedisPoolUtil --> RedisShardedPoolUtil (换成Redis集群)
            String executionResult = RedisShardedPoolUtil.setEx(session.getId(), JsonUtil.obj2String(response.getData()), Const.RedisCacheExTime.REDIS_SESSION_EXTIME);
            if (executionResult == null) {//Redis服务未启动时 正常返回OK
                response = ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), "Redis服务异常，保存数据失败");
            }
        } else {
            session.removeAttribute(Const.CURRENT_USER);//已经登录的，再次登录时
            //涉及到需要登录的接口应该统一设计一下密码修改状态的监听
        }
        return response;
    }

    @RequestMapping(value = "logout.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> logout(/*HttpSession session,*/ HttpServletRequest request, HttpServletResponse response) {
        // session.removeAttribute(Const.CURRENT_USER);//三期代码注释
        String loginToken = CookieUtil.readLoginToken(request);
        CookieUtil.delLoginToken(request, response);//Cookie中删除
        //RedisPoolUtil --> RedisShardedPoolUtil (换成Redis集群)
        RedisShardedPoolUtil.del(loginToken);//Redis中删除
        return ServerResponse.createBySuccess();
    }

    @RequestMapping(value = "register.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> register(User user) {
        return iUserService.register(user);
    }

    @RequestMapping(value = "check_valid.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> checkValid(String str, String type) {
        return iUserService.checkValid(str, type);
    }

    @RequestMapping(value = "get_user_info.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> getUserInfo(/*HttpSession session,*/ HttpServletRequest request) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user != null)
            return ServerResponse.createBySuccess(user);
        return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户的信息");
    }

    //未登录
    @RequestMapping(value = "forget_get_question.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> forgetGetQuestion(String username) {
        return iUserService.selectQuestion(username);
    }

    //未登录
    @RequestMapping(value = "forget_check_answer.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> forgetCheckAnswer(String username, String question, String answer) {
        return iUserService.checkAnswer(username, question, answer);
    }

    //未登录
    @RequestMapping(value = "forget_reset_password.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> forgetResetPassword(String username, String passwordNew, String forgetToken) {
        return iUserService.forgetResetPassword(username, passwordNew, forgetToken);
    }

    @RequestMapping(value = "reset_password.do", method = RequestMethod.POST)
    @ResponseBody
    //已登录 重置密码
    public ServerResponse<String> resetPassword(/*HttpSession session,*/ HttpServletRequest request, String passwordOld, String passwordNew) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null) {
            return ServerResponse.createByErrorMessage("用户未登录"); //TODO 要指定错误码 10
        }
        return iUserService.resetPassword(passwordOld, passwordNew, user);
    }

    //    特别注意:
//    SpringMVC提交参数是为实体时User user，获取字段均为null的处理方案
//    1>加上注解@RequestBody User user
//    提交时使用json字符串（并指定application/json）,jQuery提交时也要注意转成字符串
//    2> 使用x-www-form-urlencoded提交，使用form-data时字段为null，
//    与添不添加Headers（Content-Type   application/x-www-form-urlencoded）似乎无关
//    相关接口:
//    http://localhost:8080/user/update_information.do
    @RequestMapping(value = "update_information.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> update_information(/*HttpSession session,*/ HttpServletRequest request, User user) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
        ServerResponse<User> userFromRedisCache = iUserService.getUserByRedisCache(request);
        User currentUser = userFromRedisCache.getData();
        if (currentUser == null) {
            return ServerResponse.createByErrorMessage("用户未登录"); //TODO 要指定错误码 10
        }
        user.setId(currentUser.getId()); //user 是待更新的对象
        user.setUsername(currentUser.getUsername()); //以上这两步都关键，防止关键信息被修改,
        // 同时也用于下面response.getData()时会有这些关键信息
        ServerResponse<User> response = iUserService.updateInformation(user);
        if (response.isSuccess()) {
            response.getData().setUsername(currentUser.getUsername());
            //  session.setAttribute(Const.CURRENT_USER, response.getData()); //注意这里使用response.getData()
            // 这里使用getMsg() 保存sessionId 是getUserByRedisCache() 内部的自定义的协议
            //RedisPoolUtil --> RedisShardedPoolUtil (换成Redis集群)
            RedisShardedPoolUtil.setEx(userFromRedisCache.getMsg(), JsonUtil.obj2String(response.getData()), Const.RedisCacheExTime.REDIS_SESSION_EXTIME);
        }
        return response;//这里错误的话可以直接返回
    }

    @RequestMapping(value = "get_information.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> get_information(/*HttpSession session,*/ HttpServletRequest request) {
        //User currentUser = (User) session.-getAttribute(Const.CURRENT_USER);
        User currentUser = iUserService.getUserByRedisCache(request).getData();
        if (currentUser == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "未登录，需要强制登录status=10"); //TODO 要指定错误码 10
        }
        return iUserService.getInformation(currentUser.getId());
    }
}
