package com.mmall.controller.backend;

import com.google.common.collect.Maps;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Product;
import com.mmall.service.IFileService;
import com.mmall.service.IProductService;
import com.mmall.service.IUserService;
import com.mmall.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Description：
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/20
 */
@Controller
@RequestMapping("/manage/product")
public class ProductManageController {

    @Autowired
    private IUserService iUserService;
    @Autowired
    private IProductService iProductService;
    @Autowired
    private IFileService iFileService;

    @RequestMapping("save.do")
    @ResponseBody
    public ServerResponse productSave(/*HttpSession session,*/ HttpServletRequest request, Product product) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录，请管理员登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            return iProductService.saveOrUpdateProduct(product);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iProductService.saveOrUpdateProduct(product);
    }

    @RequestMapping("set_sale_status.do")
    @ResponseBody
    public ServerResponse setSaleStatus(/*HttpSession session,*/ HttpServletRequest request, Integer productId, Integer status) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录，请管理员登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            return iProductService.setSaleStatus(productId, status);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iProductService.setSaleStatus(productId, status);
    }

    @RequestMapping("detail.do")
    @ResponseBody
    public ServerResponse getDetail(/*HttpSession session,*/ HttpServletRequest request, Integer productId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录，请管理员登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            return iProductService.manageProductDetail(productId);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iProductService.manageProductDetail(productId);
    }

    @RequestMapping("list.do")
    @ResponseBody
    public ServerResponse getList(/*HttpSession session,*/ HttpServletRequest request, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum, @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录，请管理员登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            return iProductService.getProductList(pageNum, pageSize);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iProductService.getProductList(pageNum, pageSize);
    }

    @RequestMapping("search.do")
    @ResponseBody
    public ServerResponse productSearch(/*HttpSession session,*/ HttpServletRequest request, String productName, Integer productId, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum, @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录，请管理员登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            return iProductService.searchProduct(productName, productId, pageNum, pageSize);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iProductService.searchProduct(productName, productId, pageNum, pageSize);
    }

    @RequestMapping("upload.do")
    @ResponseBody//不加注解时，input的name需要为file
    public ServerResponse upload(/*HttpSession session,*/  @RequestParam(value = "upload_file", required = false) MultipartFile file, HttpServletRequest request) {
        //SpringMVC文件上传的相关配置在dispatcher-servlet.xml
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录，请管理员登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//
//            String path = request.getSession().getServletContext().getRealPath("upload");//发布后在webapp下 这里实测path = null
//            path = this.getClass().getResource("/").getPath().replaceAll("^\\/", "") + "upload";
//            //getServletContext.getRealPath() 为null
//            //  http://blog.csdn.net/itguaicai/article/details/42583167
//            // D:/workspace/idea/mmall/target/mmall/WEB-INF/classes/upload
//            String targetFileName = iFileService.upload(file, path);
//            String url = PropertiesUtil.getProperty("ftp.server.http.prefix") + targetFileName;
//            Map fileMap = Maps.newHashMap();
//            fileMap.put("uri", targetFileName);
//            fileMap.put("url", url);
//            return ServerResponse.createBySuccess(fileMap);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作");
//        }
        //产品模块从7-7 6:28 秒开始 听讲解SpringMVC 配置上传文件

        //全部通过拦截器验证是否登录以及权限

        String tempUploadedFileFolder = request.getSession().getServletContext().getRealPath("upload");//发布后在webapp下 这里实测path = null
        tempUploadedFileFolder = this.getClass().getResource("/").getPath().replaceAll("^\\/", "") + "upload";
        String ftpTargetFolder = "images";//ftp服务器的子文件夹
        String targetFileName = iFileService.upload(file, tempUploadedFileFolder, ftpTargetFolder);
        String url = new StringBuilder().append(PropertiesUtil.getProperty("ftp.server.http.prefix")).append(ftpTargetFolder).append("/").append(targetFileName).toString();
        Map fileMap = Maps.newHashMap();
        fileMap.put("uri", targetFileName);
        fileMap.put("url", url);
        return ServerResponse.createBySuccess(fileMap);
    }

    @RequestMapping("richtext_img_upload.do")
    @ResponseBody//不加注解时，input的name需要为file
    public Map richtextImgUpload(/*HttpSession session,*/ @RequestParam(value = "upload_file", required = false) MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
        //富文本上传有自己的返回值要求
        //  {
        //     "success":true/false,
        //      msg":"error message",//optional
        //      "file_path":"[real img url]"
        //   }
        Map resultMap = Maps.newHashMap();
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            resultMap.put("success", false);
//            resultMap.put("msg", "请登录管理员");
//            return resultMap;
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            String path = request.getSession().getServletContext().getRealPath("upload");//发布后在webapp下  这里实测path = null
//            path = this.getClass().getResource("/").getPath().replaceAll("^\\/", "") + "upload";
//            String targetFileName = iFileService.upload(file, path);
//            if (org.apache.commons.lang3.StringUtils.isBlank(targetFileName)) {
//                resultMap.put("success", false);
//                resultMap.put("msg", "上传失败");
//                return resultMap;
//            }
//            String url = PropertiesUtil.getProperty("ftp.server.http.prefix") + targetFileName;
//            resultMap.put("success", true);
//            resultMap.put("msg", "上传成功");
//            resultMap.put("file_path", url);
//            response.addHeader("Access-Control-Allow-Headers", "X-File-Name");//富文本插件要求
//            return resultMap;
//        } else {
//            resultMap.put("success", false);
//            resultMap.put("msg", "无操作权限");
//            return resultMap;
//        }

        //全部通过拦截器验证是否登录以及权限
        String tempUploadedFileFolder = request.getSession().getServletContext().getRealPath("upload");//发布后在webapp下  这里实测path = null
        tempUploadedFileFolder = this.getClass().getResource("/").getPath().replaceAll("^\\/", "") + "upload";
        String ftpTargetFolder = "images";//ftp服务器的子文件夹
        String targetFileName = iFileService.upload(file, tempUploadedFileFolder, ftpTargetFolder);
        if (org.apache.commons.lang3.StringUtils.isBlank(targetFileName)) {
            resultMap.put("success", false);
            resultMap.put("msg", "上传失败");
            return resultMap;
        }
        String url = new StringBuilder().append(PropertiesUtil.getProperty("ftp.server.http.prefix")).
                append(ftpTargetFolder).append("/").append(targetFileName).toString();
        resultMap.put("success", true);
        resultMap.put("msg", "上传成功");
        resultMap.put("file_path", url);
        response.addHeader("Access-Control-Allow-Headers", "X-File-Name");//富文本插件要求
        return resultMap;
    }
}
