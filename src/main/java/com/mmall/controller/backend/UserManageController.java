package com.mmall.controller.backend;

import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import com.mmall.util.CookieUtil;
import com.mmall.util.JsonUtil;
import com.mmall.util.RedisShardedPoolUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2017/11/9.
 */
@Controller
@RequestMapping("/manage/user") //到底加不加/？
public class UserManageController {
    @Autowired
    private IUserService iUserService;

    @RequestMapping(value = "/login.do", method = RequestMethod.POST) //到底加不加/？
    @ResponseBody
    public ServerResponse<User> login(String username, String password, HttpSession session, HttpServletResponse httpServletResponse) {
        ServerResponse<User> response = iUserService.login(username, password);
        if (response.isSuccess()) {
            User user = response.getData();
            if (user.getRole() == Const.Role.ROLE_ADMIN) {
                //管理员登录
                //session.setAttribute(Const.CURRENT_USER, user);
                //Const.CURRENT_USER 为自定义产量类，后期应该和cookie结合，
                //有cookie时也给登录并写入到session，这一过程都统一在拦截器中处理
                CookieUtil.writeLoginToken(httpServletResponse, session.getId());
                //CookieUtil.readLoginToken(httpServletRequest);
                //CookieUtil.delLoginToken(httpServletRequest,httpServletResponse);
                String executionResult = RedisShardedPoolUtil.setEx(session.getId(), JsonUtil.obj2String(response.getData()), Const.RedisCacheExTime.REDIS_SESSION_EXTIME);
                if (executionResult == null) {//Redis服务未启动时 正常返回OK
                    response = ServerResponse.createByErrorCodeMessage(ResponseCode.INNER_ERROR.getCode(), "Redis服务异常，保存数据失败");
                }
            } else {
                ServerResponse.createByErrorMessage("不是管理员，无法登陆");
            }
        }
        return response; //不是success，直接返回
    }
}
