package com.mmall.controller.backend;

import com.mmall.common.ServerResponse;
import com.mmall.service.ICategoryService;
import com.mmall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Description：后台品类管理控制器
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2017/11/11
 */
@Controller
@RequestMapping("/manage/category")
public class CategoryManageController {

    @Autowired
    private IUserService iUserService;
    @Autowired
    private ICategoryService iCategoryService;


    @RequestMapping(value = "add_category.do")
    @ResponseBody
    public ServerResponse addCategory(/*HttpSession session,*/ HttpServletRequest request, String categoryName, @RequestParam(value = "parentId", defaultValue = "0") int parentId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            //是管理员
//            //处理增加分类
//            return iCategoryService.addCategory(categoryName, parentId);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作,需要管理员权限");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iCategoryService.addCategory(categoryName, parentId);
    }

    @RequestMapping(value = "set_category_name.do")
    @ResponseBody
    public ServerResponse setCategoryName(/*HttpSession session,*/ HttpServletRequest request, Integer categoryId, String categoryName) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            //是管理员
//            //更新分类名称
//            return iCategoryService.updateCategoryName(categoryId, categoryName);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作,需要管理员权限");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iCategoryService.updateCategoryName(categoryId, categoryName);
    }

    @RequestMapping(value = "get_category.do")
    @ResponseBody
    public ServerResponse getChildrenParallelCategory(/*HttpSession session,*/ HttpServletRequest request, @RequestParam(value = "categoryId", defaultValue = "0") Integer categoryId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            //是管理员
//            //查询子节点的分类信息，并且不递归，保持平级
//            return iCategoryService.getChildrenParallelCategory(categoryId);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作,需要管理员权限");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iCategoryService.getChildrenParallelCategory(categoryId);
    }

    @RequestMapping(value = "get_deep_category.do")
    @ResponseBody
    public ServerResponse getCategoryAndDeepChildrenCategory(/*HttpSession session,*/ HttpServletRequest request, @RequestParam(value = "categoryId", defaultValue = "0") Integer categoryId) {
        //User user = (User) session.-getAttribute(Const.CURRENT_USER);
//        User user = iUserService.getUserByRedisCache(request).getData();
//
//        if (user == null) {
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录,请登录");
//        }
//        if (iUserService.checkAdminRole(user).isSuccess()) {
//            //是管理员
//            //查询当前节点的id和递归子节点的id
//            // 0 -> 1000 -> 10000 如果传1000,则需要返回10000，如果传0，则1000和10000都要返回
//            return iCategoryService.selectCategoryAndChildrenById(categoryId);
//        } else {
//            return ServerResponse.createByErrorMessage("无权操作,需要管理员权限");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iCategoryService.selectCategoryAndChildrenById(categoryId);
    }

}