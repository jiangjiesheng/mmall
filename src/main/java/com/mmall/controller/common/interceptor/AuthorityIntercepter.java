package com.mmall.controller.common.interceptor;

import com.google.common.collect.Maps;
import com.mmall.common.Const;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import com.mmall.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

/**
 * Description：登录授权拦截器
 * Author：江节胜
 * Email：dev@jiangjiesheng.cn
 * Date: 2018/3/2
 */
@Slf4j
public class AuthorityIntercepter implements HandlerInterceptor {
    @Autowired
    private IUserService iUserService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("preHandle ");
        // 请求中Controller中的方法
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        //解析HandlerMethod
        String methodName = handlerMethod.getMethod().getName();
        String calssName = handlerMethod.getBean().getClass().getSimpleName();
        //如果不同的包中与有相同的类名，则使用getName()来替换全路径的名称
        //解析参数，具体的参数key以及value是什么，打印日志

        StringBuilder requestParamBuffer = new StringBuilder();
        Map paramMap = request.getParameterMap();
        Iterator it = paramMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String mapKey = (String) entry.getKey();
            String mapValue = StringUtils.EMPTY;

            //request 参数的map，里面的value返回的是一个String[]
            Object obj = entry.getValue();
            if (obj instanceof String[]) {
                String[] strs = (String[]) obj;
                mapValue = Arrays.toString(strs);
            }
            requestParamBuffer.append(mapKey).append("=").append(mapValue);
        }

        if (StringUtils.equals(calssName, "UserManageController") &&
                StringUtils.equals(methodName, "login")) {
            log.info("权限拦截器拦截到请求，className:{},methodName:{}", calssName, methodName);
            //如果是拦截登录请求，则不打印参数，会打印到日志中，防止泄露
            return true;//true 传递请求 ，false 不传递 ，返回false 则不会调用controller中方法，和安卓中的事件冒泡 正好相反
        }
        log.info("权限拦截器拦截到请求，className:{},methodName:{},param:{}", calssName, methodName, requestParamBuffer.toString());

        User user = iUserService.getUserByRedisCache(request).getData();
        if (user == null || (user.getRole() != Const.Role.ROLE_ADMIN)) {
            response.reset();//这里添加reset(),否则报异常 getWriter() has already been called for this response
            response.setCharacterEncoding("UTF-8");//必须
            response.setContentType("application/json;charset=UTF-8");

            PrintWriter out = response.getWriter();
            if (user == null) {
                if (StringUtils.equals(calssName, "ProductManageController") &&
                        StringUtils.equals(methodName, "richtextImgUpload")) {//特殊处理
                    Map resultMap = Maps.newHashMap();
                    resultMap.put("success", false);
                    resultMap.put("msg", "请登录管理员");
                    out.print(JsonUtil.obj2String(resultMap));
                } else {
                    out.print(JsonUtil.obj2String(ServerResponse.createByErrorMessage("拦截器拦截，用户未登录")));
                }
            } else {
                if (StringUtils.equals(calssName, "ProductManageController") &&
                        StringUtils.equals(methodName, "richtextImgUpload")) {//特殊处理
                    Map resultMap = Maps.newHashMap();
                    resultMap.put("success", false);
                    resultMap.put("msg", "无操作权限");
                    out.print(JsonUtil.obj2String(resultMap));
                } else {
                    out.print(JsonUtil.obj2String(ServerResponse.createByErrorMessage("拦截器拦截，用户非管理员")));
                }
            }
            out.flush();
            out.close();
            return false;//返回false 则不会调用controller中方法
        }
        return true;//true 传递请求 ，false 不传递 ，返回false 则不会调用controller中方法，和安卓中的事件冒泡 正好相反
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("postHandle ");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("afterCompletion -所有的处理完成返回ModelAndView(非前后端分离)");
    }
}
